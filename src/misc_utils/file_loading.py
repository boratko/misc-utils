from typing import *
from pathlib import Path

from loguru import logger

__all__ = [
    "filepath_with_potential_extensions",
    "find_file",
]

from typing import Union, Optional


def filepath_with_potential_extensions(
    filepath: Union[str, Path], suffixes: Iterable[str] = ("", ".xz",),
) -> Path:
    """
    Returns the first file path found to exist with one of the suffixes provided.
    :param filepath: File path to check.
    :param suffixes: Iterable of potential suffixes to use.
        (Remember to include "" if you want to check the file itself.)
    :return: Path of existing file
    :raises: FileNotFoundError if no filepath with one of the existing suffixes exist
    """
    suffixes = list(suffixes)
    for suffix in suffixes:
        new_filepath = Path(str(filepath) + suffix)
        if new_filepath.exists():
            break
    if new_filepath.exists():
        return new_filepath
    else:
        raise FileNotFoundError(
            f"No file {filepath} with an additional suffix in {suffixes} exists."
        )


def find_file(
    data_dir: Union[Path, str] = Path("."),
    default_filename: Optional[str] = None,
    override_filename: Optional[str] = None,
    suffixes: Iterable[str] = ("", ".xz"),
    must_exist: bool = False,
) -> Optional[Path]:
    """
    Looks for override_filename in a given directory, if not found uses default_filename.
    :param data_dir: Directory to look for files in
    :param default_filename: Default filename to use, if override_filename is not provided
    :param override_filename: Manually specified filename
    :param suffixes: Potential suffixes for either default or override file
    :param must_exist: If False, just warn if default_filename is found, if True raise FileNotFoundError
    :return: File path which is found, if one exists, otherwise None
    :raises: FileNotFoundError if override_filename is specified or must_exist is True and matching file is not found
    """
    data_dir = Path(data_dir)
    if override_filename is not None:
        return filepath_with_potential_extensions(
            data_dir / override_filename, suffixes
        )
    else:
        try:
            return filepath_with_potential_extensions(
                data_dir / default_filename, suffixes
            )
        except FileNotFoundError:
            if must_exist is False:
                logger.warning(
                    f"Unable to locate {data_dir / default_filename}, proceeding without it."
                )
                return None
            else:
                raise
